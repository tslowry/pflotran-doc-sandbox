.. _gas-qa-tests:

Gas QA Tests
============

Steady Gas (Pressure)
----------------------
* :ref:`gas-steady-1D-pressure-BC-1st-kind`

* :ref:`gas-steady-1D-pressure-BC-1st-2nd-kind`

* :ref:`gas-steady-2D-pressure-BC-1st-2nd-kind`

* :ref:`gas-steady-3D-pressure-BC-2nd-kind`
