.. _installation:

*******************
Installing PFLOTRAN
*******************

**PFLOTRAN is on** `Bitbucket`_ 

PFLOTRAN can be installed on Linux, Mac, and Windows systems. 
We provide instructions for these systems in the links below. 
Please note, a Windows installer is not yet available. PFLOTRAN
can only be used on Windows either via Cygwin, or Visual Studio.

* :ref:`linux-install`

* :ref:`mac-install`

* :ref:`windows-cygwin-gnu-install`

* :ref:`windows-visual-studio-install`

* :ref:`legacy-build-install`

* :ref:`previous-petsc-releases`

* :ref:`machine-specific-petsc-configs`


.. _Bitbucket: https://bitbucket.org/pflotran/pflotran/wiki/Home


 
