import sys
from h5py import *
import numpy as np

filename = 'dataset.h5'
h5file = File(filename,mode='w')

# 1d line in x
# Pressure initial condition [Pa]
L = 100.
h5grp = h5file.create_group('initial')
h5grp.attrs['Dimension'] = np.string_('X')
# Delta length between points [m]
h5grp.attrs['Discretization'] = [1.]
# Location of origin
h5grp.attrs['Origin'] = [0.]
# Load the dataset values
nx = 101
p_offset = 0.101325   # [MPa]
rarray = np.zeros(nx,'=f8')
for i in range(nx):
  if (0. <= i < (L/10.)):
    rarray[i] = ( 0. + p_offset ) * 1.0e6  # [Pa]
  if ((L/10.) <= i < (4.*L/10.)):
    rarray[i] = ( (10./(3.*L))*float(i) - (1./3.) + p_offset ) * 1.0e6  # [Pa]
  if ((4.*L/10.) <= i < (6.*L/10.)):
    rarray[i] = ( 1. + p_offset ) * 1.0e6  # [Pa]
  if ((6.*L/10.) <= i < (9.*L/10.)):
    rarray[i] = ( 3. - (10./(3.*L))*float(i) + p_offset ) * 1.0e6  # [Pa]
  if ((9.*L/10.) <= i < L):
    rarray[i] = ( 0. + p_offset ) * 1.0e6  # [Pa]
h5dset = h5grp.create_dataset('Data', data=rarray)