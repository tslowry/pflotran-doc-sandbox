.. _flow-transient-1D-pressure-BC-2nd-kind:

*********************************************
1D Transient Flow (Pressure), BCs of 2nd Kind 
*********************************************
:ref:`flow-transient-1D-pressure-BC-2nd-kind-description`

:ref:`flow-general-transient-1D-pressure-BC-2nd-kind-pflotran-input`

:ref:`flow-th-transient-1D-pressure-BC-2nd-kind-pflotran-input`

:ref:`flow-richards-transient-1D-pressure-BC-2nd-kind-pflotran-input`

:ref:`flow-transient-1D-pressure-BC-2nd-kind-python`



.. _flow-transient-1D-pressure-BC-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.2.8, pg.33, "A Transient 1D 
Pressure Distribution, Time-Dependent Boundary Conditions of 2nd Kind."

The domain is a 25x1x1 meter rectangular column extending along the positive 
x-axis and is made up of 50x1x1 cubic grid cells with dimensions 
0.5x1x1 meters. The domain material is assigned the following properties: 
porosity :math:`\phi` = 0.25; :math:`k` = 1.0e-14 m^2. The fluid viscosity 
:math:`\mu` = 0.864 mPa/sec, and the fluid compressibility :math:`K` = 1.0e-8 
1/Pa.
**The accuracy of the numerical solution**
**is very sensitive to the time step size because the fluid flux value is held**
**constant at the value at the beginning of the timestep. Reducing the timestep**
**will make the numerical solution more accurate relative to the analytical**
**solution, but note this is not an issue with time truncation error.**

The pressure is initially uniform at *p(t=0)* = 0 MPa.
At the left end, a no fluid flux boundary condition is applied, and at the right
end, a transient fluid flux boundary condition is applied (both in units of
m/sec):

.. math::
   q(0,t) = 0
   
   q(L,t) = 9.0x10^{-6} t

where L = 25 m. The transient pressure distribution is governed by,

.. math:: 
   \chi = {k \over {\phi \mu K}}

   {\phi K} {{\partial p} \over {\partial t}} = {k \over \mu} {{\partial^{2} p} \over {\partial x^{2}}}

With the given boundary conditions, the solution is defined by,

.. math::
   p(x,t) = {{8q\sqrt{\chi t^3}} \over {k \over \mu}} \sum_{n=0}^{\infty} \left[{ i^3erfc{{(2n+1)L-x}\over{2\sqrt{\chi t}}} + i^3erfc{{(2n+1)L+x}\over{2\sqrt{\chi t}}} }\right]
   
where :math:`i^3erfc(g)` represents the third repeated integral of the
complimentary error function, given by,

.. math::
   i^3erfc(g) = {2 \over \pi} \int^{\infty}_{g} {{(s-g)^3}\over{3!}} e^{-s^2} ds

.. figure:: ../qa_tests/flow/transient/1D/BC_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/flow/transient/1D/BC_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.
      
.. figure:: ../qa_tests/flow/transient/1D/BC_2nd_kind/richards_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for RICHARDS mode.
   
   
   
.. _flow-general-transient-1D-pressure-BC-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_2nd_kind/general_mode/1D_transient_pressure_BC_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_2nd_kind/general_mode/1D_transient_pressure_BC_2nd_kind.in



.. _flow-th-transient-1D-pressure-BC-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_2nd_kind/th_mode/1D_transient_pressure_BC_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_2nd_kind/th_mode/1D_transient_pressure_BC_2nd_kind.in



.. _flow-richards-transient-1D-pressure-BC-2nd-kind-pflotran-input:

The PFLOTRAN Input File (RICHARDS Mode)
=======================================
The RICHARDS Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/flow/transient/1D/BC_2nd_kind/richards_mode/1D_transient_pressure_BC_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/flow/transient/1D/BC_2nd_kind/richards_mode/1D_transient_pressure_BC_2nd_kind.in


  
.. _flow-transient-1D-pressure-BC-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: flow_transient_1D_BC2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
