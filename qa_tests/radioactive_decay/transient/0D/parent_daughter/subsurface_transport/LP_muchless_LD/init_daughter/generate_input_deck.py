import sys

nx = 1; dx = 1; lx = 10
ny = 1; dy = 1; ly = 10
nz = 1; dz = 1; lz = 10
#filename = ''
filename = 'pd_init_LP_muchless_LD_sst.in'
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
    /
  /
END

"""
chemistry = """
CHEMISTRY
  PRIMARY_SPECIES
    Am-241
    Dtr
  /
  RADIOACTIVE_DECAY_REACTION
    REACTION Am-241 <-> Dtr
    HALF_LIFE 432.7 y
  /
  RADIOACTIVE_DECAY_REACTION
    REACTION Dtr <->
    HALF_LIFE 2140000 y
  /

  TRUNCATE_CONCENTRATION 1.d-21
  DATABASE ../../../../../../Decay_base.dat

  OUTPUT
    TOTAL
    Am-241
    Dtr
  /
END
"""
transport_solver_options = """
NEWTON_SOLVER TRANSPORT
  RTOL 1.d-08
  ATOL 1.d-12
  MINIMUM_NEWTON_ITERATIONS 3
/
"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  BOUNDS
   0.d0 0.d0 0.d0
   """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0 
  END
END
"""

region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""

mat_prop = """
MATERIAL_PROPERTY test_rock
  ID 1
  SATURATION_FUNCTION default
  POROSITY 0.018d0
  TORTUOSITY 1.d-2
  ROCK_DENSITY 2700.d0
  THERMAL_CONDUCTIVITY_DRY 2.9d0
  THERMAL_CONDUCTIVITY_WET 2.9d0
  HEAT_CAPACITY 950.d0
  PERMEABILITY
    PERM_ISO 1.0d-15 #m*m
  /
/
"""

strata = """
STRATA
  REGION all
  MATERIAL test_rock
END
"""
time = """
TIME
  FINAL_TIME 19000000.d0 y
  INITIAL_TIMESTEP_SIZE 100.d0 y
  MAXIMUM_TIMESTEP_SIZE 100.d0 y at 0. y
  MAXIMUM_TIMESTEP_SIZE 1.d5 y at 3500. y
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    PERIODIC TIMESTEP 1
    FORMAT HDF5
  /
END
"""
flow_cond = """
TRANSPORT_CONDITION initial
  TYPE dirichlet
  CONSTRAINT_LIST initial
    0.d0 initial
  /
/

CONSTRAINT initial
  CONCENTRATIONS
    Am-241    100  T
    Dtr       100  T
  /
/
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  TRANSPORT_CONDITION initial
END
"""

observation = """
OBSERVATION
  REGION all
  AT_CELL_CENTER
/
"""

f.write(simulation)
f.write("SUBSURFACE\n")
f.write(chemistry)
f.write(transport_solver_options)
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(strata)
f.write(time)
f.write(output)
f.write(flow_cond)
f.write(init_cond)
f.write(observation)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
