import os
import sys
import qa_tests_engine as qa
import numpy as np


# Possible command line arguments
d_0D = False
d_1D = False
d_2D = False
d_3D = False
stdy = False
trns = False
thrm = False
flow = False
gas = False
rad = False
th = False
gen = False
rich = False
trps = False
remove = False
screen_on = False
pf_exe = ''
mpi_exe = 'mpirun'
num_tries = 3
arg_help = False

path = ''
cwd = ''
nxyz = np.zeros(3) # [nx,ny,nz]
os.system('rm report.txt')

for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-1D':
    d_1D = True
  elif sys.argv[k] == '-2D':
    d_2D = True
  elif sys.argv[k] == '-3D':
    d_3D = True
  elif sys.argv[k] == '-0D':
    d_0D = True
  elif sys.argv[k] == '-STEADY':
    stdy = True
  elif sys.argv[k] == '-TRANSIENT':
    trns = True
  elif sys.argv[k] == '-THERMAL':
    thrm = True
  elif sys.argv[k] == '-FLOW':
    flow = True
  elif sys.argv[k] == '-GAS':
    gas = True
  elif sys.argv[k] == '-RADIOACTIVE_DECAY':
    rad = True
  elif sys.argv[k] == '-TH_MODE':
    th = True
  elif sys.argv[k] == '-GENERAL_MODE':
    gen = True
  elif sys.argv[k] == '-RICHARDS_MODE':
    rich = True
  elif sys.argv[k] == '-TRANSPORT':
    trps = True 
  elif sys.argv[k] == '-ALL':
    d_1D = True
    d_2D = True
    d_3D = True
  elif sys.argv[k][0:11] == '-NUM_TRIES=':
    num_tries = int(float(sys.argv[k][11:]))
  elif sys.argv[k] == '-REMOVE':
    remove = True
  elif sys.argv[k] == '-SCREEN_ON':
    screen_on = True
  elif sys.argv[k][0:3] == '-E=':
    pf_exe = sys.argv[k][3:]
  elif sys.argv[k][0:5] == '-MPI=':
    mpi_exe = sys.argv[k][5:]
  elif sys.argv[k] == '-HELP':
    arg_help = True
  else:
    print '\nERROR: Unrecognized argument ' + sys.argv[k]
    print '       If you need help, use argument -HELP'
    exit()
    
################################################################################

print ' '
print '========================================================================='
print '====== PFLOTRAN QA Test Suite ==========================================='
print '========================================================================='
print '=========================================================================' 
print 'Arguments: ' + str(sys.argv[1:])
print ' '
if arg_help:
  print ' Usage: \n'
  print ' The -E flag is required and must indicate the path to the '
  print ' PFLOTRAN executable which will run the QA tests.\n '
  print '   -E=path/to/PFLOTRAN/executable \n'
  print ' If mpirun is not the MPI executable you desire, give the '
  print ' correct path to the executable using the -MPI flag.\n'
  print '   -MPI=path/to/MPI/executable \n'
  print ' At least one of the following flags must also be given: \n'
  print '   -ALL'
  print '   -1D             -2D             -3D'
  print '   -GENERAL_MODE   -RICHARDS_MODE  -TH_MODE'
  print '   -TRANSPORT'
  print '   -THERMAL        -FLOW           -GAS'
  print '   -STEADY         -TRANSIENT'
  print '   -NUM_TRIES='
  print '   -REMOVE'
  print '   -SCREEN_ON'
  print '   -HELP'
  print ' '
  exit()
if pf_exe == '':
  print '\nERROR: Path to PFLOTRAN executable was not provided using the -E flag.'
  print '       If you need help, use argument -HELP'
  exit()
if len(sys.argv) < 3:
  print '\nERROR: At least one flag must be specified, in addition to -E=.'
  print '       If you need help, use argument -HELP'
  exit()

################################################################################
#### STEADY THERMAL TESTS ######################################################
################################################################################

if d_1D or stdy or thrm or th:
  print '=============================================='
  print '  1D, Steady, Thermal, BCs 1st kind, TH Mode'  
  print '==============================================' 
  os.chdir('thermal/steady/1D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_steady_1D_BC1stkind(cwd,'1D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

  print '=================================================='
  print '  1D, Steady, Thermal, BCs 1st/2nd kind, TH Mode'  
  print '==================================================' 
  os.chdir('thermal/steady/1D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_steady_1D_BC1st2ndkind(cwd,'1D_steady_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries) 
  os.chdir('../../../../..')

if d_2D or stdy or thrm or th:
  print '=============================================='
  print '  2D, Steady, Thermal, BCs 1st kind, TH Mode'
  print '=============================================='
  os.chdir('thermal/steady/2D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_steady_2D_BC1stkind(cwd,'2D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

  print '=================================================='
  print '  2D, Steady, Thermal, BCs 1st/2nd kind, TH Mode'  
  print '==================================================' 
  os.chdir('thermal/steady/2D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_steady_2D_BC1st2ndkind(cwd,'2D_steady_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

if d_3D or stdy or thrm or th:
  print '=============================================='
  print '  3D, Steady, Thermal, BCs 1st kind, TH Mode'
  print '=============================================='
  os.chdir('thermal/steady/3D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_steady_3D_BC1stkind(cwd,'3D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########

if d_1D or stdy or thrm or gen:
  print '==================================================='
  print '  1D, Steady, Thermal, BCs 1st kind, GENERAL Mode'  
  print '===================================================' 
  os.chdir('thermal/steady/1D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_steady_1D_BC1stkind(cwd,'1D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

  print '======================================================'
  print '  1D, Steady, Thermal, BCs 1st/2nd kind, GENERAL Mode'  
  print '======================================================' 
  os.chdir('thermal/steady/1D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_steady_1D_BC1st2ndkind(cwd,'1D_steady_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

if d_2D or stdy or thrm or gen:
  print '==================================================='
  print '  2D, Steady, Thermal, BCs 1st kind, GENERAL Mode'
  print '==================================================='
  os.chdir('thermal/steady/2D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_steady_2D_BC1stkind(cwd,'2D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

  print '======================================================='
  print '  2D, Steady, Thermal, BCs 1st/2nd kind, GENERAL Mode'  
  print '=======================================================' 
  os.chdir('thermal/steady/2D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_steady_2D_BC1st2ndkind(cwd,'2D_steady_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

if d_3D or stdy or thrm or gen:
  print '===================================================='
  print '  3D, Steady, Thermal, BCs 1st kind, GENERAL Mode'
  print '===================================================='
  os.chdir('thermal/steady/3D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_steady_3D_BC1stkind(cwd,'3D_steady_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

################################################################################
#### STEADY TRANSPORT TESTS ####################################################
################################################################################

if d_1D or stdy or trps:
  print '======================================================='
  print '  1D, Steady, Transport, BCs 1st kind'
  print '======================================================='
  os.chdir('transport/steady/1D/BC_1st_kind/subsurface_transport'); cwd = os.getcwd()
  qa.transport_steady_1D_BC1stkind(cwd,'1D_steady_transport_BC_1st_kind',
                                     remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
####
if d_2D or stdy or trps:
  print '=============================================='
  print '  2D, Steady, Transport, BCs 1st kind'
  print '=============================================='
  os.chdir('transport/steady/2D/BC_1st_kind/subsurface_transport'); cwd = os.getcwd()
  qa.transport_steady_2D_BC1stkind(cwd,'2D_steady_transport_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
####
if d_3D or stdy or trps:
  print '===================================================='
  print '  3D, Steady, Transport, BCs 1st kind'
  print '===================================================='
  os.chdir('transport/steady/3D/BC_1st_kind/subsurface_transport'); cwd = os.getcwd()
  qa.transport_steady_3D_BC1stkind(cwd,'3D_steady_transport_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

################################################################################
#### TRANSIENT TRANSPORT TESTS #################################################
################################################################################

if d_1D or trns or trps:
  print '========================================================'
  print '  1D, Transient, Transport, IC'
  print '========================================================'
  os.chdir('transport/transient/1D/IC/subsurface_transport'); cwd = os.getcwd()
  qa.transport_transient_1D_IC(cwd,'transient_1D_IC_subsurface_transport',
                               remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#  print '========================================================'
#  print '  1D, Transient, Transport, IC with Flow: Pe = 225'
#  print '========================================================'
#  os.chdir('transport/transient/1D/IC_with_flow/subsurface_transport/Pe_225')
#  cwd = os.getcwd()
#  qa.transport_transient_1D_IC_with_flow_pe1(cwd,
#                           'transient_1D_IC_with_flow_subsurface_transport_pe1',
#                                      remove,screen_on,pf_exe,mpi_exe,num_tries)
#  os.chdir('../../../../../..')
  
#  print '========================================================'
#  print '  1D, Transient, Transport, IC with Flow: Pe = 32'
#  print '========================================================'
#  os.chdir('transport/transient/1D/IC_with_flow/subsurface_transport/Pe_32')
#  cwd = os.getcwd()
#  qa.transport_transient_1D_IC_with_flow_pe2(cwd,
#                           'transient_1D_IC_with_flow_subsurface_transport_pe2',
#                                      remove,screen_on,pf_exe,mpi_exe,num_tries)
#  os.chdir('../../../../../..')
  
#  print '========================================================'
#  print '  1D, Transient, Transport, IC with Flow: Pe = 32,500'
#  print '========================================================'
#  os.chdir('transport/transient/1D/IC_with_flow/subsurface_transport/Pe_32500')
#  cwd = os.getcwd()
#  qa.transport_transient_1D_IC_with_flow_pe3(cwd,
#                           'transient_1D_IC_with_flow_subsurface_transport_pe3',
#                                      remove,screen_on,pf_exe,mpi_exe,num_tries)
#  os.chdir('../../../../../..')

################################################################################
#### TRANSIENT THERMAL TESTS ###################################################
################################################################################

if d_1D or trns or thrm or th:
  print '================================================='
  print '  1D, Transient, Thermal, BCs 1st kind, TH Mode'
  print '================================================='
  os.chdir('thermal/transient/1D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC1stkind(cwd,'1D_transient_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '====================================================='
  print '  1D, Transient, Thermal, BCs 1st/2nd kind, TH Mode'
  print '====================================================='
  os.chdir('thermal/transient/1D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC1st2ndkind(cwd,'1D_transient_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '================================================='
  print '  1D, Transient, Thermal, BCs 2nd kind, TH Mode'
  print '================================================='
  os.chdir('thermal/transient/1D/BC_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC2ndkind(cwd,'1D_transient_thermal_BC_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or trns or thrm or th:
  print '====================================================='
  print '  2D, Transient, Thermal, BCs 1st/2nd kind, TH Mode'
  print '====================================================='
  os.chdir('thermal/transient/2D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.thermal_transient_2D_BC1st2ndkind(cwd,'2D_transient_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########

if d_1D or trns or thrm or gen:
  print '======================================================'
  print '  1D, Transient, Thermal, BCs 1st kind, GENERAL Mode'
  print '======================================================'
  os.chdir('thermal/transient/1D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC1stkind(cwd,'1D_transient_thermal_BC_1st_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '=========================================================='
  print '  1D, Transient, Thermal, BCs 1st/2nd kind, GENERAL Mode'
  print '=========================================================='
  os.chdir('thermal/transient/1D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC1st2ndkind(cwd,'1D_transient_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '======================================================'
  print '  1D, Transient, Thermal, BCs 2nd kind, GENERAL Mode'
  print '======================================================'
  os.chdir('thermal/transient/1D/BC_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_transient_1D_BC2ndkind(cwd,'1D_transient_thermal_BC_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or trns or thrm or gen:
  print '=========================================================='
  print '  2D, Transient, Thermal, BCs 1st/2nd kind, GENERAL Mode'
  print '=========================================================='
  os.chdir('thermal/transient/2D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.thermal_transient_2D_BC1st2ndkind(cwd,'2D_transient_thermal_BC_1st_2nd_kind',
				      remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
################################################################################
#### STEADY FLOW TESTS #########################################################
################################################################################

if d_1D or stdy or flow or rich:
  print '================================================='
  print '  1D, Steady, Flow, BCs 1st kind, RICHARDS Mode'  
  print '=================================================' 
  os.chdir('flow/steady/1D/BC_1st_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1stkind(cwd,'1D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '====================================================='
  print '  1D, Steady, Flow, BCs 1st/2nd kind, RICHARDS Mode'  
  print '=====================================================' 
  os.chdir('flow/steady/1D/BC_1st_2nd_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1st2ndkind(cwd,'1D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '================================================'
  print '  1D, Steady, Flow, Hydrostatic, RICHARDS Mode'  
  print '================================================' 
  os.chdir('flow/steady/1D/hydrostatic/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_hydrostatic(cwd,'1D_steady_pressure_hydrostatic',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or stdy or flow or rich:
  print '================================================='
  print '  2D, Steady, Flow, BCs 1st kind, RICHARDS Mode'  
  print '=================================================' 
  os.chdir('flow/steady/2D/BC_1st_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1stkind(cwd,'2D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '====================================================='
  print '  2D, Steady, Flow, BCs 1st/2nd kind, RICHARDS Mode'  
  print '=====================================================' 
  os.chdir('flow/steady/2D/BC_1st_2nd_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1st2ndkind(cwd,'2D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_3D or stdy or flow or rich:
  print '================================================='
  print '  3D, Steady, Flow, BCs 1st kind, RICHARDS Mode'  
  print '=================================================' 
  os.chdir('flow/steady/3D/BC_1st_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_steady_3D_BC1stkind(cwd,'3D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########

if d_1D or stdy or flow or th:
  print '==========================================='
  print '  1D, Steady, Flow, BCs 1st kind, TH Mode'  
  print '===========================================' 
  os.chdir('flow/steady/1D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1stkind(cwd,'1D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '==============================================='
  print '  1D, Steady, Flow, BCs 1st/2nd kind, TH Mode'  
  print '===============================================' 
  os.chdir('flow/steady/1D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1st2ndkind(cwd,'1D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '=========================================='
  print '  1D, Steady, Flow, Hydrostatic, TH Mode'  
  print '==========================================' 
  os.chdir('flow/steady/1D/hydrostatic/th_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_hydrostatic(cwd,'1D_steady_pressure_hydrostatic',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or stdy or flow or th:
  print '==========================================='
  print '  2D, Steady, Flow, BCs 1st kind, TH Mode'  
  print '===========================================' 
  os.chdir('flow/steady/2D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1stkind(cwd,'2D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '==============================================='
  print '  2D, Steady, Flow, BCs 1st/2nd kind, TH Mode'  
  print '===============================================' 
  os.chdir('flow/steady/2D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1st2ndkind(cwd,'2D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_3D or stdy or flow or th:
  print '==========================================='
  print '  3D, Steady, Flow, BCs 1st kind, TH Mode'  
  print '===========================================' 
  os.chdir('flow/steady/3D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.flow_steady_3D_BC1stkind(cwd,'3D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########

if d_1D or stdy or flow or gen:
  print '================================================'
  print '  1D, Steady, Flow, BCs 1st kind, GENERAL Mode'  
  print '================================================' 
  os.chdir('flow/steady/1D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1stkind(cwd,'1D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '===================================================='
  print '  1D, Steady, Flow, BCs 1st/2nd kind, GENERAL Mode'  
  print '====================================================' 
  os.chdir('flow/steady/1D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_BC1st2ndkind(cwd,'1D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '==============================================='
  print '  1D, Steady, Flow, Hydrostatic, GENERAL Mode'  
  print '===============================================' 
  os.chdir('flow/steady/1D/hydrostatic/general_mode'); cwd = os.getcwd()
  qa.flow_steady_1D_hydrostatic(cwd,'1D_steady_pressure_hydrostatic',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or stdy or flow or gen:
  print '================================================'
  print '  2D, Steady, Flow, BCs 1st kind, GENERAL Mode'  
  print '================================================' 
  os.chdir('flow/steady/2D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1stkind(cwd,'2D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '===================================================='
  print '  2D, Steady, Flow, BCs 1st/2nd kind, GENERAL Mode'  
  print '====================================================' 
  os.chdir('flow/steady/2D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.flow_steady_2D_BC1st2ndkind(cwd,'2D_steady_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_3D or stdy or flow or gen:
  print '================================================'
  print '  3D, Steady, Flow, BCs 1st kind, GENERAL Mode'  
  print '================================================' 
  os.chdir('flow/steady/3D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.flow_steady_3D_BC1stkind(cwd,'3D_steady_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
################################################################################
#### TRANSIENT FLOW TESTS ######################################################
################################################################################
  
if d_1D or trns or flow or rich:
  print '===================================================='
  print '  1D, Transient, Flow, BCs 1st kind, RICHARDS Mode'  
  print '====================================================' 
  os.chdir('flow/transient/1D/BC_1st_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1stkind(cwd,'1D_transient_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')  
  
  print '===================================================='
  print '  1D, Transient, Flow, BCs 2nd kind, RICHARDS Mode'  
  print '====================================================' 
  os.chdir('flow/transient/1D/BC_2nd_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC2ndkind(cwd,'1D_transient_pressure_BC_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '========================================================'
  print '  1D, Transient, Flow, BCs 1st/2nd kind, RICHARDS Mode'  
  print '========================================================' 
  os.chdir('flow/transient/1D/BC_1st_2nd_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1st2ndkind(cwd,'1D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or trns or flow or rich:
  print '========================================================'
  print '  2D, Transient, Flow, BCs 1st/2nd kind, RICHARDS Mode'  
  print '========================================================' 
  os.chdir('flow/transient/2D/BC_1st_2nd_kind/richards_mode'); cwd = os.getcwd()
  qa.flow_transient_2D_BC1st2ndkind(cwd,'2D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########
  
if d_1D or trns or flow or th:
  print '=============================================='
  print '  1D, Transient, Flow, BCs 1st kind, TH Mode'  
  print '==============================================' 
  os.chdir('flow/transient/1D/BC_1st_kind/th_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1stkind(cwd,'1D_transient_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '=============================================='
  print '  1D, Transient, Flow, BCs 2nd kind, TH Mode'  
  print '==============================================' 
  os.chdir('flow/transient/1D/BC_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC2ndkind(cwd,'1D_transient_pressure_BC_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '=================================================='
  print '  1D, Transient, Flow, BCs 1st/2nd kind, TH Mode'  
  print '==================================================' 
  os.chdir('flow/transient/1D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1st2ndkind(cwd,'1D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or trns or flow or th:
  print '=================================================='
  print '  2D, Transient, Flow, BCs 1st/2nd kind, TH Mode'  
  print '==================================================' 
  os.chdir('flow/transient/2D/BC_1st_2nd_kind/th_mode'); cwd = os.getcwd()
  qa.flow_transient_2D_BC1st2ndkind(cwd,'2D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
#########
  
if d_1D or trns or flow or gen:
  print '==================================================='
  print '  1D, Transient, Flow, BCs 1st kind, GENERAL Mode'  
  print '===================================================' 
  os.chdir('flow/transient/1D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1stkind(cwd,'1D_transient_pressure_BC_1st_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '==================================================='
  print '  1D, Transient, Flow, BCs 2nd kind, GENERAL Mode'  
  print '===================================================' 
  os.chdir('flow/transient/1D/BC_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC2ndkind(cwd,'1D_transient_pressure_BC_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '======================================================='
  print '  1D, Transient, Flow, BCs 1st/2nd kind, GENERAL Mode'  
  print '=======================================================' 
  os.chdir('flow/transient/1D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.flow_transient_1D_BC1st2ndkind(cwd,'1D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or trns or flow or gen:
  print '======================================================='
  print '  2D, Transient, Flow, BCs 1st/2nd kind, GENERAL Mode'  
  print '=======================================================' 
  os.chdir('flow/transient/2D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.flow_transient_2D_BC1st2ndkind(cwd,'2D_transient_pressure_BC_1st_2nd_kind',
			              remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
################################################################################
#### STEADY GAS TESTS ##########################################################
################################################################################

if d_1D or stdy or gas or gen:
  print '==============================================='
  print '  1D, Steady, Gas, BCs 1st kind, GENERAL Mode'  
  print '===============================================' 
  os.chdir('gas/steady/1D/BC_1st_kind/general_mode'); cwd = os.getcwd()
  qa.gas_steady_1D_BC1stkind(cwd,'1D_steady_gas_BC_1st_kind',
				 remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
  print '==================================================='
  print '  1D, Steady, Gas, BCs 1st/2nd kind, GENERAL Mode'  
  print '===================================================' 
  os.chdir('gas/steady/1D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.gas_steady_1D_BC1st2ndkind(cwd,'1D_steady_gas_BC_1st_2nd_kind',
				 remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_2D or stdy or gas or gen:
  print '==================================================='
  print '  2D, Steady, Gas, BCs 1st/2nd kind, GENERAL Mode'  
  print '===================================================' 
  os.chdir('gas/steady/2D/BC_1st_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.gas_steady_2D_BC1st2ndkind(cwd,'2D_steady_gas_BC_1st_2nd_kind',
				 remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
if d_3D or stdy or gas or gen:
  print '==============================================='
  print '  3D, Steady, Gas, BCs 2nd kind, GENERAL Mode'  
  print '===============================================' 
  os.chdir('gas/steady/3D/BC_2nd_kind/general_mode'); cwd = os.getcwd()
  qa.gas_steady_3D_BC2ndkind(cwd,'3D_steady_gas_BC_2nd_kind',
				 remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')
  
################################################################################
#### RADIOACTIVE DECAY TESTS ###################################################
################################################################################

if d_0D or rad:
  print '==============================================='
  print '  CHANGE ME'  
  print '===============================================' 
  os.chdir('radioactive_decay/etc/etc/etc/. . . .'); cwd = os.getcwd()
#  qa.gas_steady_3D_BC2ndkind(cwd,'3D_steady_gas_BC_2nd_kind',
#				 remove,screen_on,pf_exe,mpi_exe,num_tries)
  os.chdir('../../../../..')

#########

print ' '
print ' DONE running all tests.'
print ' '
cwd = os.getcwd(); qa.print_report(cwd)
print ' '

