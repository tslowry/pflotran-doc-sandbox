import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE GENERAL
    /
  /
END

"""
grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION left_end
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION right_end
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""
mat_prop = """
MATERIAL_PROPERTY beam
  ID 1
  POROSITY 1.d-15
  TORTUOSITY 1.d0
  ROCK_DENSITY 2.0d3 kg/m^3
  HEAT_CAPACITY 0.01 J/kg-C
  THERMAL_CONDUCTIVITY_DRY 0.5787037 W/m-C
  THERMAL_CONDUCTIVITY_WET 0.5787037 W/m-C
  CHARACTERISTIC_CURVES cc1
  PERMEABILITY
    PERM_X 1.d-25
    PERM_Y 1.d-25
    PERM_Z 1.d-25
  /
END
"""
char_curves = """
CHARACTERISTIC_CURVES cc1
  SATURATION_FUNCTION VAN_GENUCHTEN
    LIQUID_RESIDUAL_SATURATION 0.5d-1
    M 0.75
    ALPHA 1.d-3
  /
  PERMEABILITY_FUNCTION MUALEM
    LIQUID_RESIDUAL_SATURATION 0.1d0
    M 0.5d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_GAS
    LIQUID_RESIDUAL_SATURATION 0.1d0
    GAS_RESIDUAL_SATURATION 0.1d0
    M 0.5d0
  /
END
"""
fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""
strata = """
STRATA
  REGION all
  MATERIAL beam
END
"""
time = """
TIME
  FINAL_TIME 0.50 day
  INITIAL_TIMESTEP_SIZE 1.d-4 day
  MAXIMUM_TIMESTEP_SIZE 0.001 day at 0.d0 day
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES day 0.015 0.05 0.1
    #NO_PRINT_INITIAL
    FORMAT HDF5
  /
END
"""
dataset = """
DATASET temperature_initial
  HDF5_DATASET_NAME initial
  FILENAME ../dataset.h5
END
"""
flow_cond = """
FLOW_CONDITION initial
  TYPE
    TEMPERATURE dirichlet
    LIQUID_PRESSURE dirichlet
    MOLE_FRACTION dirichlet
  /
  TEMPERATURE DATASET temperature_initial
  LIQUID_PRESSURE 101325 Pa
  MOLE_FRACTION 1.d-10
END

FLOW_CONDITION left_end
  TYPE
    ENERGY_FLUX neumann
    LIQUID_FLUX neumann
    GAS_FLUX neumann
  /
  ENERGY_FLUX 0.d0 W/m^2
  LIQUID_FLUX 0.d0 m/yr
  GAS_FLUX 0.d0 m/yr
END

FLOW_CONDITION right_end
  TYPE
    ENERGY_FLUX neumann
    LIQUID_FLUX neumann
    GAS_FLUX neumann
  /
  ENERGY_FLUX 0.d0 W/m^2
  LIQUID_FLUX 0.d0 m/yr
  GAS_FLUX 0.d0 m/yr
END
"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  FLOW_CONDITION initial
END

BOUNDARY_CONDITION left_end
  REGION left_end
  FLOW_CONDITION left_end
END

BOUNDARY_CONDITION right_end
  FLOW_CONDITION right_end
  REGION right_end
END
"""
  
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(char_curves)
f.write(strata)
f.write(fluid_prop)
f.write(time)
f.write(output)
f.write(dataset)
f.write(flow_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work