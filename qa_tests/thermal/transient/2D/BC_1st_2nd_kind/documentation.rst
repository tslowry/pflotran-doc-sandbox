.. _thermal-transient-2D-conduction-BC-1st-2nd-kind:

******************************************************
2D Transient Thermal Conduction, BCs of 1st & 2nd Kind
******************************************************
:ref:`thermal-transient-2D-conduction-BC-1st-2nd-kind-description`

:ref:`thermal-general-transient-2D-conduction-BC-1st-2nd-kind-pflotran-input`

:ref:`thermal-th-transient-2D-conduction-BC-1st-2nd-kind-pflotran-input`

:ref:`thermal-transient-2D-conduction-BC-1st-2nd-kind-dataset`

:ref:`thermal-transient-2D-conduction-BC-1st-2nd-kind-python`



.. _thermal-transient-2D-conduction-BC-1st-2nd-kind-description:

The Problem Description
=======================

This problem is adapted from *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.9, pg.23, "A Transient 2D 
Temperature Distribution, Non-Zero Initial Temperature, Boundary Conditions 
of 1st and 2nd Kind."

The domain is a 100x100x1 meter square plate extending along the positive 
x and y axis and is made up of 50x50x1 hexahedral grid cells with dimensions 
2x2x1 meters. The domain is composed of a single material and is assigned the 
following properties: thermal conductivity *K* = 0.5787037 W/(m-C); specific 
heat capacity *Cp* = 0.01 J/(m-C); density :math:`\rho` = 2,000 kg/m^3.

The temperature is initially distributed according to

.. math:: 
   T(t=0) = T_b f(x) f(y) + T_{offset}
   
   f(x) = \sum_{n=1}^{\infty} \left({80 \over {3(n\pi)^2}}\right)sin{{n\pi} \over 2} sin{{n \pi x} \over L} sin{{n\pi} \over 4} sin{{3n\pi} \over 20} 
   
   f(y) = {1 \over 2} + \sum_{n=1}^{\infty} \left({80 \over {3(n\pi)^2}}\right) cos{{n \pi y} \over L} cos{{n\pi} \over 2} sin{{n\pi} \over 4} sin{{3n\pi} \over 20} 

   \chi = {K \over {\rho c_p}}

where :math:`T_b` = 1C and :math:`T_{offset}` = 0.1 C. 
The boundary conditions are:

.. math:: 
   T(0,y) = 0        \hspace{0.25in} x=0 \hspace{0.15in} face
  
   T(L,y) = 0        \hspace{0.25in} x=L \hspace{0.15in} face
  
   q(x,0) = 0        \hspace{0.25in} y=0 \hspace{0.15in} face
  
   q(x,L) = 0        \hspace{0.25in} y=L \hspace{0.15in} face

The transient temperature distribution is governed by,

.. math:: 
  \rho c_p {{\partial T} \over {\partial t}} = K \left({ {{\partial^{2} T} \over {\partial x^{2}}} + {{\partial^{2} T} \over {\partial y^{2}}} }\right)

The solution is given by,

.. math:: 

   T(x,y,t) = T_b f(x,t) f(y,t) + T_{offset}
   
   f(x) = \sum_{n=1}^{\infty} exp\left({-\chi n^2 \pi^2 {t \over L^2}}\right)\left({80 \over {3(n\pi)^2}}\right) sin{{n \pi x} \over L} sin{{n\pi} \over 2} sin{{n\pi} \over 4} sin{{3n\pi} \over 20} 
   
   f(y) = {1 \over 2} + \sum_{n=1}^{\infty} exp\left({-\chi n^2 \pi^2 {t \over L^2}}\right)\left({80 \over {3(n\pi)^2}}\right) cos{{n \pi y} \over L} cos{{n\pi} \over 2} sin{{n\pi} \over 4} sin{{3n\pi} \over 20} 

   \chi = {K \over {\rho c_p}} 
   
.. figure:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/visit_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.

.. figure:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/general_mode/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for GENERAL mode.
   
.. figure:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/th_mode/comparison_plot.png
   :width: 49 %
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for TH mode.


   
.. _thermal-general-transient-2D-conduction-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (GENERAL Mode)
======================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/general_mode/2D_transient_thermal_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/general_mode/2D_transient_thermal_BC_1st_2nd_kind.in



.. _thermal-th-transient-2D-conduction-BC-1st-2nd-kind-pflotran-input:

The PFLOTRAN Input File (TH Mode)
=================================
The TH Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/th_mode/2D_transient_thermal_BC_1st_2nd_kind.in>`.

.. literalinclude:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/th_mode/2D_transient_thermal_BC_1st_2nd_kind.in



.. _thermal-transient-2D-conduction-BC-1st-2nd-kind-dataset:

The Dataset
===========
The hdf5 dataset required to define the initial/boundary conditions is created
with the following python script called ``create_dataset.py``:

.. literalinclude:: ../qa_tests/thermal/transient/2D/BC_1st_2nd_kind/create_dataset.py
 
 

.. _thermal-transient-2D-conduction-BC-1st-2nd-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: thermal_transient_2D_BC1st2ndkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
