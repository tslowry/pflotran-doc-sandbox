.. _transport-steady-1D-BC-1st-kind:

*********************************************
1D Steady Transport, BCs of 1st Kind 
*********************************************
:ref:`transport-steady-1D-BC-1st-kind-description`

:ref:`transport-subsurface_transport-steady-1D-BC-1st-kind-pflotran-input`

:ref:`transport-steady-1D-BC-1st-kind-python`



.. _transport-steady-1D-BC-1st-kind-description:

The Problem Description
=======================

This problem is based off of *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.1, pg.14, "A 1D Steady-State 
Temperature Distribution, Boundary Conditions of 1st Kind." Concentrations
are used in place of temperatures to simulate subsurface transport, solute 
diffusion only

The domain is a 100x10x10 meter rectangular beam extending along the positive 
x-axis and is made up of 10x1x1 cubic grid cells with dimensions 10x10x10 
meters. The domain material is assigned the following properties: diffusion
coefficeint *C* = 1.0E-9 m^2/s

The concentration is initially uniform at *C(c=0)* = 1.5M.
The boundary concentrations are *C(x=0)* = 1.0M and *C(x=L)* = 2.0M, where L = 100 m.
The simulation is run until the steady-state concentration field
develops. 

The LaPlace equation governs the steady-state concentration field,

.. math:: {{\partial^{2} C} \over {\partial x^{2}}} = 0

The solution is given by,

.. math:: C(x) = ax + b

When the boundary conditions *C(x=0)* = 1.0M and *C(x=L)* = 2.0M are applied,
the solution becomes,

.. math:: C(x) = C(x=0){x \over L}

.. figure:: ../qa_tests/transport/steady/1D/BC_1st_kind/paraview_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/transport/steady/1D/BC_1st_kind/subsurface_transport/comparison_plot.png
   :width: 49 %  
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for SUBSURFACE_TRANSPORT mode.
   
   

.. _transport-subsurface_transport-steady-1D-BC-1st-kind-pflotran-input:   

The PFLOTRAN Input File (SUBSURFACE_TRANSPORT Mode)
===================================================
The General Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/transport/steady/1D/BC_1st_kind/subsurface_transport/1D_steady_transport_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/transport/steady/1D/BC_1st_kind/subsurface_transport/1D_steady_transport_BC_1st_kind.in



  
.. _transport-steady-1D-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: transport_steady_1D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
