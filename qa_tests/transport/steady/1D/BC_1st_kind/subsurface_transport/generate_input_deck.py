import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
    /
  /
END

"""

chemistry = """
CHEMISTRY
  PRIMARY_SPECIES
    tracer
  /
  OUTPUT
    TOTAL
    ALL
  /
END

"""

linear_solver = """

LINEAR_SOLVER TRANSPORT
  SOLVER DIRECT
END

"""


grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION left_end
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION right_end
  FACE EAST
  COORDINATES
    """ + str(lx) + """d0 0.d0 0.d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END
"""

mat_prop = """
MATERIAL_PROPERTY beam
  ID 1
  POROSITY 0.01d0
  TORTUOSITY 1.d0
END
"""

strata = """
STRATA
  REGION all
  MATERIAL beam
END
"""
fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""
time = """
TIME
  FINAL_TIME 105145 yr
  INITIAL_TIMESTEP_SIZE 1.d0 yr
  MAXIMUM_TIMESTEP_SIZE 2.d0 yr at 0.d0 yr
END
"""
output = """
OUTPUT
  SNAPSHOT_FILE
    TIMES yr 20000
    NO_PRINT_INITIAL
    FORMAT HDF5
    FORMAT VTK
  /
END
"""
transport_cond = """
TRANSPORT_CONDITION initial
  TYPE DIRICHLET
  CONSTRAINT initial
    CONCENTRATIONS
      tracer 1.5d0    T
    /
  /
END

TRANSPORT_CONDITION left_end
  TYPE DIRICHLET
  CONSTRAINT left_end
    CONCENTRATIONS
      tracer 1.0d0    T
    /
  /
END

TRANSPORT_CONDITION right_end
  TYPE DIRICHLET
  CONSTRAINT right_end
    CONCENTRATIONS
      tracer 2.0d0    T
    /
  /
END

"""
init_cond = """
INITIAL_CONDITION initial
  REGION all
  TRANSPORT_CONDITION initial
END

BOUNDARY_CONDITION left_end
  REGION left_end
  TRANSPORT_CONDITION left_end
END

BOUNDARY_CONDITION right_end
  TRANSPORT_CONDITION right_end
  REGION right_end
END
"""
  
  
f.write(simulation)
f.write("SUBSURFACE\n")
f.write(linear_solver)
f.write(chemistry)
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(fluid_prop)
f.write(time)
f.write(output)
f.write(strata)
f.write(transport_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
