.. _transport-steady-2D-BC-1st-kind:

*********************************************
2D Steady Transport, BCs of 1st Kind 
*********************************************
:ref:`transport-steady-2D-BC-1st-kind-description`

:ref:`transport-subsurface_transport-steady-2D-BC-1st-kind-pflotran-input`

:ref:`transport-steady-2D-BC-1st-kind-python`


.. _transport-steady-2D-BC-1st-kind-description:

The Problem Description
=======================

This problem is based off of *Kolditz, et al. (2015), 
Thermo-Hydro-Mechanical-Chemical Processes in Fractured Porous Media: 
Modelling and Benchmarking, Closed Form Solutions, Springer International 
Publishing, Switzerland.* Section 2.1.3, pg.16, "A 2D Steady-State 
Temperature Distribution, Boundary Conditions of 1st Kind." Concentrations 
are used in place of temperatures to simulate subsurface transport, solute
diffusion only.

The domain is a 1x1x1 meter slab extending along the positive 
x-axis and y-axis and is made up of 10x10x1 hexahedral grid cells with 
dimensions 0.1x0.1x1 meters. The domain material is assigned the following 
properties: diffusion coefficeint *C* = 1.0E-9 m^2/s;

The concentration is initially uniform at *C(c=0)* = 1.0 M.
The boundary concentrations are:

.. math::
  C(0,y) = 0 \hspace{0.25in} x=0 \hspace{0.15in} face
  
  C(L,y) = C0 {y \over L}        \hspace{0.25in} x=L \hspace{0.15in} face
  
  C(x,0) = 0 \hspace{0.25in} y=0 \hspace{0.15in} face
  
  C(x,L) = C0 {x \over L}        \hspace{0.25in} y=L \hspace{0.15in} face

where L = 1 m and C0 = 1.0 M.
The simulation is run until the steady-state concentration field
develops. 

The LaPlace equation governs the steady-state concentration field,

.. math:: {{\partial^{2} C} \over {\partial x^{2}}} + {{\partial^{2} C} \over {\partial y^{2}}}= 0

The solution is given by,

.. math:: C(x,y) = C_{c=0} {x \over L}{y \over L}

.. figure:: ../qa_tests/transport/steady/2D/BC_1st_kind/paraview_figure.png
   :width: 55 %
   
   The PFLOTRAN domain set-up.
   
.. figure:: ../qa_tests/transport/steady/2D/BC_1st_kind/subsurface_transport/comparison_plot.png
   :width: 49 %   
   :alt: If you do not see this image, you must run the QA test suite to generate this figure.
   
   Comparison of the PFLOTRAN vs. analytical solution for SUBSURFACE_TRANSPORT mode.
      

   
.. _transport-subsurface_transport-steady-2D-BC-1st-kind-pflotran-input:

The PFLOTRAN Input File (SUBSURFACE_TRANSPORT)
==============================================
The Subsurface_transport Mode PFLOTRAN input file can be downloaded 
:download:`here <../qa_tests/transport/steady/2D/BC_1st_kind/subsurface_transport/2D_steady_transport_BC_1st_kind.in>`.

.. literalinclude:: ../qa_tests/transport/steady/2D/BC_1st_kind/subsurface_transport/2D_steady_transport_BC_1st_kind.in


  
.. _transport-steady-2D-BC-1st-kind-python:

The Python Script
=================

.. literalinclude:: ../qa_tests/qa_tests_engine.py
  :pyobject: transport_steady_2D_BC1stkind
  
Refer to section :ref:`python-helper-functions` for documentation on the 
``qa_tests_helper`` module, which defines the helper functions used in the
Python script above.
