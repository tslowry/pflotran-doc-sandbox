import sys

nx = 1; dx = 1; lx = 1
ny = 1; dy = 1; ly = 1
nz = 1; dz = 1; lz = 1
vx = 0.9;
filename = ''
for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-nx':
    nx = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-ny':
    ny = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-nz':
    nz = int(float(sys.argv[k+1]))
  if sys.argv[k] == '-dx':
    dx = float(sys.argv[k+1])
  if sys.argv[k] == '-dy':
    dy = float(sys.argv[k+1])
  if sys.argv[k] == '-dz':
    dz = float(sys.argv[k+1])
  if sys.argv[k] == '-lx':
    lx = float(sys.argv[k+1])
  if sys.argv[k] == '-ly':
    ly = float(sys.argv[k+1])
  if sys.argv[k] == '-lz':
    lz = float(sys.argv[k+1])
  if sys.argv[k] == '-input_prefix':
    filename = sys.argv[k+1] + '.in'
    
f = open(filename,'w')

simulation = """ 
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
    /
  /
END

"""
chemistry = """
CHEMISTRY
  PRIMARY_SPECIES
    tracer
  /
  OUTPUT
    TOTAL
    ALL
  /
END
"""
linear_solver = """
LINEAR_SOLVER TRANSPORT
  SOLVER DIRECT
END
"""

grid = """
GRID
  TYPE structured
  NXYZ """ + str(nx) + " " + str(ny) + " " + str(nz) + """
  DXYZ
   """ + str(dx) + """d0
   """ + str(dy) + """d0
   """ + str(dz) + """d0
  END
END
"""
region = """
REGION all
  COORDINATES
    0.0d0 0.0d0 0.0d0
    """ + str(lx) + "d0 " + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION west_face
  FACE WEST
  COORDINATES
    0.d0 0.d0 0.d0
    0.d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION east_face1
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.0d0 0.0d0
    """ + str(lx) + """d0 0.1d0 0.0d0
  /
END

REGION east_face2
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.1d0 0.0d0
    """ + str(lx) + """d0 0.2d0 0.0d0
  /
END

REGION east_face3
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.2d0 0.0d0
    """ + str(lx) + """d0 0.3d0 0.0d0
  /
END

REGION east_face4
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.3d0 0.0d0
    """ + str(lx) + """d0 0.4d0 0.0d0
  /
END

REGION east_face5
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.4d0 0.0d0
    """ + str(lx) + """d0 0.5d0 0.0d0
  /
END

REGION east_face6
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.5d0 0.0d0
    """ + str(lx) + """d0 0.6d0 0.0d0
  /
END

REGION east_face7
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.6d0 0.0d0
    """ + str(lx) + """d0 0.7d0 0.0d0
  /
END

REGION east_face8
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.7d0 0.0d0
    """ + str(lx) + """d0 0.8d0 0.0d0
  /
END

REGION east_face9
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.8d0 0.0d0
    """ + str(lx) + """d0 0.9d0 0.0d0
  /
END

REGION east_face10
  FACE EAST
  COORDINATES
    """ + str(vx) + """d0 0.9d0 0.0d0
    """ + str(lx) + """d0 1.0d0 0.0d0
  /
END

REGION north_face1
  FACE NORTH
  COORDINATES
    0.d0 """ + str(vx) + """d0 0.d0
    0.1d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION north_face2
  FACE NORTH
  COORDINATES
    0.1d0 """ + str(vx) + """d0 0.0d0
    0.2d0 """ + str(ly) + "d0 " + str(lz) + """d0  
  /
END

REGION north_face3
  FACE NORTH
  COORDINATES
    0.2d0 """ + str(vx) + """d0 0.0d0
    0.3d0 """ + str(ly) + "d0 " + str(lz) + """d0   
  /
END

REGION north_face4
  FACE NORTH
  COORDINATES
    0.3d0 """ + str(vx) + """d0 0.0d0
    0.4d0 """ + str(ly) + "d0 " + str(lz) + """d0 
  /
END

REGION north_face5
  FACE NORTH 
  COORDINATES
    0.4d0 """ + str(vx) + """d0 0.0d0
    0.5d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION north_face6
  FACE NORTH
  COORDINATES
    0.5d0 """ + str(vx) + """d0 0.0d0
    0.6d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION north_face7
  FACE NORTH
  COORDINATES
    0.6d0 """ + str(vx) + """d0 0.0d0
    0.7d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION 	north_face8
  FACE NORTH
  COORDINATES
    0.7d0 """ + str(vx) + """d0 0.0d0
    0.8d0 """ + str(ly) + "d0 " + str(lz) + """d0 
  /
END

REGION north_face9
  FACE NORTH 
  COORDINATES
    0.8d0 """ + str(vx) + """d0 0.0d0
    0.9d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION north_face10
  FACE NORTH
  COORDINATES
    0.9d0 """ + str(vx) + """d0 0.0d0
    1.0d0 """ + str(ly) + "d0 " + str(lz) + """d0
  /
END

REGION south_face
  FACE SOUTH
  COORDINATES
    0.d0  0.d0  0.d0
    1.0d0 0.0d0 1.0d0
  /
END
"""

mat_prop = """
MATERIAL_PROPERTY plate
  ID 1
  POROSITY 0.01d0
  TORTUOSITY 1.d0
END
"""

strata = """
STRATA
  REGION all
  MATERIAL plate
END
"""

fluid_prop = """
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END
"""

time = """
TIME
  FINAL_TIME 100000 yr
  INITIAL_TIMESTEP_SIZE 1.d-2 day
  MAXIMUM_TIMESTEP_SIZE 5.d0 yr at 0.d0 yr
END
"""

output = """
OUTPUT
  SNAPSHOT_FILE
    NO_PRINT_INITIAL
    FORMAT HDF5
    FORMAT VTK
  /
END
"""

transport_cond = """
  


TRANSPORT_CONDITION initial 
 CONSTRAINT initial
    CONCENTRATIONS
      tracer 1.0d0    T
    /
  /
END

TRANSPORT_CONDITION east_face1
  TYPE DIRICHLET
  CONSTRAINT east_face1
    CONCENTRATIONS
      tracer 0.05d0  T
    /
  /
END

TRANSPORT_CONDITION east_face2
  TYPE DIRICHLET
  CONSTRAINT east_face2
    CONCENTRATIONS
      tracer .15d0  T
    /
  /
END

TRANSPORT_CONDITION east_face3
  TYPE DIRICHLET
  CONSTRAINT east_face3
    CONCENTRATIONS
      tracer .25d0  T
    /
  /
END

TRANSPORT_CONDITION east_face4
  TYPE DIRICHLET
  CONSTRAINT east_face4
    CONCENTRATIONS
      tracer .35d0  T
    /
  /
END

TRANSPORT_CONDITION east_face5
  TYPE DIRICHLET
  CONSTRAINT east_face5
    CONCENTRATIONS
      tracer .45d0  T
    /
  /
END

TRANSPORT_CONDITION east_face6
  TYPE DIRICHLET
  CONSTRAINT east_face6
    CONCENTRATIONS
      tracer .55d0  T
    /
  /
END

TRANSPORT_CONDITION east_face7
  TYPE DIRICHLET
  CONSTRAINT east_face7
    CONCENTRATIONS
      tracer .65d0  T
    /
  /
END

TRANSPORT_CONDITION east_face8
  TYPE DIRICHLET
  CONSTRAINT east_face8
    CONCENTRATIONS
      tracer .75d0  T
    /
  /
END

TRANSPORT_CONDITION east_face9
  TYPE DIRICHLET
  CONSTRAINT east_face9
    CONCENTRATIONS
      tracer .85d0  T
    /
  /
END

TRANSPORT_CONDITION east_face10
  TYPE DIRICHLET
  CONSTRAINT east_face10
    CONCENTRATIONS
      tracer .95d0  T
    /
  /
END

TRANSPORT_CONDITION north_face1
  TYPE DIRICHLET
  CONSTRAINT north_face1
    CONCENTRATIONS
      tracer  .05d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face2
  TYPE DIRICHLET
  CONSTRAINT north_face2
    CONCENTRATIONS
      tracer  .15d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face3
  TYPE DIRICHLET
  CONSTRAINT north_face3
    CONCENTRATIONS
      tracer  .25d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face4
  TYPE DIRICHLET
  CONSTRAINT north_face4
    CONCENTRATIONS
      tracer  .35d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face5
  TYPE DIRICHLET
  CONSTRAINT north_face5
    CONCENTRATIONS
      tracer  .45d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face6
  TYPE DIRICHLET
  CONSTRAINT north_face6
    CONCENTRATIONS
      tracer  .55d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face7
  TYPE DIRICHLET
  CONSTRAINT north_face7
    CONCENTRATIONS
      tracer  .65d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face8
  TYPE DIRICHLET
  CONSTRAINT north_face8
    CONCENTRATIONS
      tracer  .75d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face9
  TYPE DIRICHLET
  CONSTRAINT north_face9
    CONCENTRATIONS
      tracer  .85d0  T
    /   
  /
END

TRANSPORT_CONDITION north_face10
  TYPE DIRICHLET
  CONSTRAINT north_face10
    CONCENTRATIONS
      tracer  .95d0  T
    /   
  /
END

TRANSPORT_CONDITION west_face
  TYPE DIRICHLET
  CONSTRAINT west_face
    CONCENTRATIONS
      tracer 0.0d0    T
    /
  /  
END

TRANSPORT_CONDITION south_face
  TYPE DIRICHLET
  CONSTRAINT south_face
    CONCENTRATIONS
      tracer 0.0d0    T
    /
  /
END
"""

init_cond = """
INITIAL_CONDITION initial
  REGION all
  TRANSPORT_CONDITION initial
END

BOUNDARY_CONDITION east_face1
  REGION east_face1
  TRANSPORT_CONDITION east_face1
END

BOUNDARY_CONDITION east_face2
  REGION east_face2
  TRANSPORT_CONDITION east_face2
END

BOUNDARY_CONDITION east_face3
  REGION east_face3
  TRANSPORT_CONDITION east_face3
END

BOUNDARY_CONDITION east_face3
  REGION east_face3
  TRANSPORT_CONDITION east_face3
END

BOUNDARY_CONDITION east_face4
  REGION east_face4
  TRANSPORT_CONDITION east_face4
END

BOUNDARY_CONDITION east_face5
  REGION east_face5
  TRANSPORT_CONDITION east_face5
END

BOUNDARY_CONDITION east_face6
  REGION east_face6
  TRANSPORT_CONDITION east_face6
END

BOUNDARY_CONDITION east_face7
  REGION east_face7
  TRANSPORT_CONDITION east_face7
END

BOUNDARY_CONDITION east_face8
  REGION east_face8
  TRANSPORT_CONDITION east_face8
END

BOUNDARY_CONDITION east_face9
  REGION east_face9
  TRANSPORT_CONDITION east_face9
END

BOUNDARY_CONDITION east_face10
  REGION east_face10
  TRANSPORT_CONDITION east_face10
END


BOUNDARY_CONDITION north_face1
  REGION north_face1
  TRANSPORT_CONDITION north_face1
END

BOUNDARY_CONDITION north_face2
  REGION north_face2
  TRANSPORT_CONDITION north_face2
END

BOUNDARY_CONDITION north_face3
  REGION north_face3
  TRANSPORT_CONDITION north_face3
END

BOUNDARY_CONDITION north_face4
  REGION north_face4
  TRANSPORT_CONDITION north_face4
END

BOUNDARY_CONDITION north_face5
  REGION north_face5
  TRANSPORT_CONDITION north_face5
END

BOUNDARY_CONDITION north_face6
  REGION north_face6
  TRANSPORT_CONDITION north_face6
END

BOUNDARY_CONDITION north_face7
  REGION north_face7
  TRANSPORT_CONDITION north_face7
END

BOUNDARY_CONDITION north_face8
  REGION north_face8
  TRANSPORT_CONDITION north_face8
END

BOUNDARY_CONDITION north_face9
  REGION north_face9
  TRANSPORT_CONDITION north_face9
END

BOUNDARY_CONDITION north_face10
  REGION north_face10
  TRANSPORT_CONDITION north_face10
END

BOUNDARY_CONDITION west_face
  REGION west_face
  TRANSPORT_CONDITION west_face
END

BOUNDARY_CONDITION south_face
  TRANSPORT_CONDITION south_face
  REGION south_face
END
"""




f.write(simulation)
f.write("SUBSURFACE\n")
f.write(chemistry)
f.write(linear_solver)
f.write(grid)
f.write(region)
f.write(mat_prop)
f.write(strata)
f.write(fluid_prop)
f.write(time)
f.write(output)
f.write(transport_cond)
f.write(init_cond)
f.write("\nEND_SUBSURFACE\n") 
# Must have \n at the end, or HDF5 output will not work
